# Excel as a Function

This is an example project to show what a very basic TypeScript FaaS wrapper
would look like when using an Excel file as a computational engine hosted on
[Vercel](https://vercel.app).

## Usage

To try this out locally, run `npm install` followed by `npm run vercel -- dev`.
