import * as XLSX from "xlsx";
import * as XLSXCalc from "xlsx-calc";
import * as formulajs from "formulajs";
import { NowRequest, NowResponse } from "@vercel/node";
import { join } from "path";

// Use Formula.js to support a wide array of default Excel formulas. This may
// not be necessary for simple Excel workbooks
XLSXCalc.import_functions(formulajs);

export default function calculateExcelResult(
  request: NowRequest,
  response: NowResponse
) {
  const workbook = XLSX.readFile(join(__dirname, "..", "engine.xlsx"));
  const apiSheet = workbook.Sheets[workbook.SheetNames[0]];

  const firstOperandCellAddress = "A1";
  const secondOperandCellAddress = "A2";

  const resultCellAddresses = "B1:E2";

  const { firstOperand, secondOperand } = request.query;

  // Update the 'input fields' in the Excel workbook
  apiSheet[firstOperandCellAddress].v = firstOperand;
  apiSheet[secondOperandCellAddress].v = secondOperand;

  // Recalculate the workbook
  XLSXCalc.XLSX_CALC(workbook);

  // Return the result cells as a JSON string. The `header` option ensures the
  // output is an 'array of arrays' representing the rows and columns in the
  // sheet
  response.status(200).json(
    XLSX.utils.sheet_to_json(apiSheet, {
      header: 1,
      range: resultCellAddresses,
    })
  );
}
